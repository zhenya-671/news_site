<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterForm;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class RegisterController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterForm $request)
    {

        $data = $request->validated();

        if ($request->has('avatar')) {
            $avatar = str_replace('public/avatars/', '', $request->file('avatar')->store('public/avatars'));
            $data['avatar'] = $avatar;

            $user = User::query()->create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'avatar' => $data['avatar'],
                'password' => bcrypt($data['password']),
            ]);
        } else {
            $user = User::query()->create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'avatar' => 'non-avatar.png',
                'password' => bcrypt($data['password']),
            ]);
        }

        if ($user) {
            auth('web')->login($user);
        }

        return redirect(route('home'));
    }
}
