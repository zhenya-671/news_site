<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NewPost;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    const PATH_TO_NEW_FILE = 'public/newPosts/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $posts = Post::query()->orderBy('id', 'DESC')->get();
        $newPosts = NewPost::query()->orderBy('id', 'desc')->get();

        return view('admin.posts.index', [
            'posts' => $posts,
            'newPosts' => $newPosts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $newPost = NewPost::query()->findOrFail($id);

        return view('admin.posts.show', [
            'newPost' => $newPost
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $post = NewPost::query()->findOrFail($id);


        $newPost = [
            'title' => $post->title,
            'description' => $post->description,
            'thumbnail' => $post->thumbnail,
            'created_at' => $post->created_at,
            'updated_at' => $post->updated_at
        ];


        if ($newPost['thumbnail'] !== null || file_exists(self::PATH_TO_NEW_FILE . $newPost['thumbnail'])) {
            Storage::move(self::PATH_TO_NEW_FILE . $newPost['thumbnail'], 'public/posts/' . $newPost['thumbnail']);
        }
        Post::query()->create($newPost);
        NewPost::destroy($id);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $post = NewPost::query()->findOrFail($id);

        Storage::delete(self::PATH_TO_NEW_FILE . $post->thumbnail);
        NewPost::destroy($id);

        return back();
    }
}
