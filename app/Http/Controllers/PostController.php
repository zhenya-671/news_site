<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentForm;
use App\Http\Requests\CreateForm;
use App\Models\NewPost;
use App\Models\Post;
use Faker\Core\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $posts = Post::query()->orderBy('created_at', 'DESC')->paginate(1);

        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {

        $post = Post::query()->findOrFail($id);

        return view('posts.partials.show', [
            'post' => $post
        ]);

    }

    /**
     * @param $id
     * @param CommentForm $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function comment($id, CommentForm $request)
    {
        $post = Post::query()->findOrFail($id);

        $post->comments()->create($request->validated());
        return redirect(route('posts.show', $id));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('posts.partials.create_post');
    }

    /**
     * @param CreateForm $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createProcess(CreateForm $request)
    {
        $post = NewPost::query();
        $data = $request->validated();

        if ($request->has('thumbnail')) {
            $thumbnail = str_replace('public/newPosts/', '', $request->file('thumbnail')->store('public/newPosts'));
            $data['thumbnail'] = $thumbnail;
        }
        $post->create($data);

        return back()->with([
                'status' => 'success'
            ]
        );

    }
}
