<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

        $posts = Post::query()->orderBy("created_at", "DESC")->limit(1)->get();

        return view('welcome', [
            'posts' => $posts
        ]);
    }
}
