<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();

        Post::factory(10)->create();

        Admin::factory([
            'firstname' => 'Admin',
            'lastname' => 'Adminovich',
            'email' => 'admin@admin.com',
            'password' => bcrypt(12345), // password'
        ])->create();
    }
}
