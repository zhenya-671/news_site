<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->realText(14),
            'description' => $this->faker->text(255),
            'thumbnail' => $this->faker->image('public/storage/posts', 1080, 720, null, false)
        ];
    }
}
