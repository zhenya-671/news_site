@extends('layouts.admin')

@section('title', 'Admin Panel')

@section('adminPanel')
    <div class="text-primary mb-3 text" id="describe">
        <h5>
            This is admin panel where admin can accept or reject new posts
        </h5>
    </div>
    <table class="table" aria-describedby="describe">
        <thead>
        <tr>
            <th scope="col">New posts</th>
            <th scope="col">
                To accept
            </th>
            <th scope="col">
                To reject
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($newPosts as $newPost)
            <tr>
                <td>
                    <a href="{{ route('admin.posts.show', $newPost->id) }}" class="link-info"
                       type="submit">{{ $newPost->title }}</a>
                </td>
                <td>
                    <form action="{{ route('admin.posts.update', $newPost->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="alert-success">Accept</button>
                    </form>
                </td>
                <td>
                    <form action="{{ route('admin.posts.destroy', $newPost->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="alert-danger">Decline</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

