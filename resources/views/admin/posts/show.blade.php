@extends('layouts.admin')

@section('title', $newPost->title)

@section('adminPanel')
    <div class="accordion-header gray">
        @if(!isset($newPost->thumbnail))
            <p>this post without image</p>
        @else
            <img src="/storage/newPosts/{{$newPost->thumbnail}}" alt="" class="img mb-3" width="1080" height="720">
        @endif
        <h3>{{$newPost->title}}</h3>
        <p>{{$newPost->description}}</p>
    </div>
@endsection
