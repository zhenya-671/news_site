@extends('layouts.app')
@section('title', 'create post')

@section('content')
    <div class="text-primary mb-3 text">
        <h5>
            If u are not seeing error after click button 'create' so all good and ur post send into administration to
            verify
        </h5>
    </div>

    <form action="{{ route('create_post_process') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="selector">U can upload image</label>
            <input type="file" name="thumbnail"
                   class="btn btn-secondary form-control mt-2" id="selector"
                   value="{{ old('thumbnail') }}"/>
        </div>
        @error('thumbnail')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Title</label>
            <input name="title" type="text" class="form-control @error('title') border-danger @enderror"
                   id="exampleFormControlInput1" placeholder="Title">
        </div>
        @error('title')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Description</label>
            <textarea name="description" class="form-control @error('description') border-danger @enderror"
                      id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        @error('description')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <button class="btn btn-primary">create</button>

    </form>

@endsection
