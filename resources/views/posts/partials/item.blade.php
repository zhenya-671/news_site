<div class="col-md-4 ">
    <div class="mb-3">
        <a href="{{route('posts.show', $post->id)}}">
            <img src="/storage/posts/{{$post->thumbnail}}" alt="" class="img" @if(isset($post->thumbnail)) width="1080"
                 height="720" @endif>
        </a>
    </div>
    <div class="mb-3">
        <h3>{{$post->title}}</h3>
    </div>
    <div class="mb-3">
        <p>{{$post->description}}</p>
    </div>
    <div class="bg-body text-secondary">
        created at
        <p class="text-secondary">{{ $post->created_at }}</p>
    </div>
    <form action="{{route('posts.show', $post->id)}}">
        @csrf
        <button class="btn btn-primary">Go there</button>
    </form>

</div>
