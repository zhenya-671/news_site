@extends('layouts.app')

@section('title', $post->title)

@section('news')
    <div class="m-5">
        @if(isset($post->thumbnail))
            <img src="/storage/posts/{{$post->thumbnail}}" alt="" class="img mb-3" width="1080" height="720">
        @endif
        <div class="m-5">
            <h3>{{$post->title}}</h3>
        </div>
        <div class="m-5">
            <p>{{$post->description}}</p>
        </div>
        <div class="bg-body text-secondary">
            created at
            <p class="text-secondary">{{ $post->created_at }}</p>
        </div>
    </div>
    @auth('web')
        <form method="post" action="{{ route('comment', $post->id) }}">
            @csrf
            <div class="row g-3 align-items-center m-5">
                <label for="exampleFormControlTextarea1" class="form-label">Comment</label>
                <textarea name="comment" class="form-control mb-3 @error('comment') border-danger @enderror"
                          id="exampleFormControlTextarea1" rows="3"></textarea>
                @error('comment')
                <p class="text-danger">{{ $message }}</p>
                @enderror
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </form>
    @endauth

        <div class="pt-4 m-5">
            @foreach($post->comments as $comment)
                <div class="p-2 bd-highlight border-1 border-danger">
                    <div class="flex flex-row justify-content-center mr-2">
                        <h3 class="text-primary text-left mt-1">
                            <img src="/storage/avatars/{{ $comment->user->avatar }}" alt=""
                                 class="img rounded-circle" width="40"
                                 height="40">
                            {{ $comment->user->firstname }}
                        </h3>
                    </div>
                    <p class="text-black text-left">{{ $comment->comment }}</p>
                </div>
            @endforeach
        </div>


@endsection
