@extends('layouts.app')

@section('title', 'Posts')

@section('news')
    <form action="{{ route('create_post') }}">
        @csrf
        <button type="submit" class="btn btn-primary mb-3">Create new post</button>
    </form>
    @foreach($posts as $post)
        @include('posts.partials.item', ['post' => $post])
    @endforeach
    <nav class="mt-3" aria-label="Page navigation example">
        {{ $posts->links() }}
    </nav>
@endsection
