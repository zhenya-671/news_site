@extends('layouts.app')
@section('title', 'Register')

@section('content')
    <form method="post" action="{{ route('register_process') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group mb-2">
            <label for="firstname">Firstname</label>
            <input type="text" name="firstname" class="form-control @error('firstname') border-danger @enderror"
                   id="firstname" value="{{ old('firstname') }}"/>
        </div>
        @error('firstname')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="form-group mb-2">
            <label for="lastname">Lastname</label>
            <input type="text" name="lastname" class="form-control @error('lastname') border-danger @enderror"
                   id="lastname" value="{{ old('lastname') }}"/>
        </div>
        @error('lastname')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="form-group mb-2">
            <label for="selector">Avatar</label>
            <input type="file" name="avatar"
                   class="btn btn-secondary form-control @error('avatar') border-danger @enderror" id="selector"/>
        </div>
        @error('avatar')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="form-group mb-2">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control @error('email') border-danger @enderror " id="email"
                   aria-describedby="emailHelp" value="{{ old('email') }}"/>
            <small id="emailHelp" class="form-text text-muted">We'll never share ur email with anyone else</small>
        </div>
        @error('email')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <div class="form-group mb-2">
            <label for="email_confirmation">Confirm email</label>
            <input type="text" name="email_confirmation" class="form-control" id="email_confirmation"
                   value="{{ old('email_confirmation') }}"/>
        </div>

        <div class="form-group mb-2">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control @error('password') border-danger @enderror"
                   id="password" aria-describedby="passwordHelp"/>
            <small id="passwordHelp" class="form-text text-muted">
                login must consist of at least 6 values: letters(1upper,1lower) special character(min 1), numbers(min 1)
            </small>
        </div>
        @error('password')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="form-group mb-2">
            <label for="password_confirmation">Confirm password</label>
            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"/>
        </div>

        <button type="submit" class="btn btn-primary">Register</button>
    </form>

@endsection
