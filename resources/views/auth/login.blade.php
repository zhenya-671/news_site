@extends('layouts.app')
@section('title', 'Login')

@section('content')
    <form action="{{ route('login_process') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group mb-2">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control @error('email') border-danger @enderror" id="email"
                   value="{{ old('email') }}"/>
        </div>
        @error('email')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <div class="form-group mb-2">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control @error('password') border-danger @enderror"
                   id="password"/>
        </div>
        @error('password')
        <p class="text-danger">{{ $message }}</p>
        @enderror

        <button type="submit" class="btn btn-primary">Login</button>

    </form>


@endsection
